package com.example.alena.wishlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by alena on 28.09.2016.
 */
public class AuthorizationFragment extends Fragment {

    private Button buttonAuthorizationGoogle;
    private FragmentManager fm;
    public static String menuString;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((MainActivity) getActivity()).hideFabButton();

        View view = inflater.inflate(R.layout.fragment_authorization, container, false);

        buttonAuthorizationGoogle = (Button) view.findViewById(R.id.button_authorization_google);

        View.OnClickListener oclAutorization = new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                fm = ((MainActivity) getActivity()).getSupportFragmentManager();
                Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);


                    fragment = new PageFragment();

                    Bundle bundle1 = new Bundle();
                    menuString = "main_page_menu";
                    bundle1.putString(MainActivity.mMENU, menuString);
                    fragment.setArguments(bundle1);

                    ((MainActivity) getActivity()).placeFragment(fragment);


            }
        };

        buttonAuthorizationGoogle.setOnClickListener(oclAutorization);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        ((MainActivity)getActivity()).showFabButton();
    }


}
