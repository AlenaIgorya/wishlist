package com.example.alena.wishlist;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by alena on 02.10.2016.
 */
public class UserRepository {

   private User alena;
   private User igor;

    private static UserRepository mUserRepository;

    public static UserRepository getObj(Context context) {
        if (mUserRepository == null) {
            mUserRepository = new UserRepository(context);
        }
        return mUserRepository;
    }

    public UserRepository(Context context) {
        alena = new User();

        alena.setNameUser("Alena Teteryatnikova");
        alena.setStatus("Ёбушки-воробушки =)");
        alena.setMail("alena.tetr@gmail.com");
        alena.setPhone("+7(915)288-58-43");
        alena.setVk("vk");
        alena.setIdentifier("I");

        igor = new User();
        igor.setNameUser("Igor Borodin");
        igor.setStatus("Счастливый");
        igor.setMail("igor.dborodin@gmail.com");
        igor.setPhone("+7(916)584-83-44");
        igor.setVk("vk");
        igor.setIdentifier("Partner");
    }

    public User getAlena(){
        return alena;
    }

    public User getIgor(){
        return igor;
    }

    public void setAlena(User user){
        alena = user;
    }



}
