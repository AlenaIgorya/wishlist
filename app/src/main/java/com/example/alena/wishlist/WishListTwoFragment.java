package com.example.alena.wishlist;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

/**
 * Created by alena on 04.10.2016.
 */
public class WishListTwoFragment extends Fragment {

    private RecyclerView mFirstWishRecyclerView;
    private RecyclerView mSecondtWishRecyclerView;
    private WishListAdapter2 mAdapterFirst;
    private WishListAdapter2 mAdapterSecond;
    private TextView mWishTitle;
    private TextView mCheckWishTitle;
    private RelativeLayout mRelativeLayoutDown1;
    private RelativeLayout mRelativeLayoutDown2;
    List<OneWishClass> mFirstWishList;
    List<OneWishClass> mSecondWishList;

    static final String TAG = "myLogs";
    public static final String EXTRA_WISH_ID = "com.example.alena.wish_id";
    private String mMenuInWLF;
    private String mMenuString;
    private String mMenuString2;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_2_wish_list, container, false);
        mFirstWishRecyclerView = (RecyclerView) view.findViewById(R.id.first_wish_list_recycler_view);
        mSecondtWishRecyclerView = (RecyclerView) view.findViewById(R.id.second_wish_list_recycler_view);
        mFirstWishRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSecondtWishRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFirstWishRecyclerView.setNestedScrollingEnabled(true);
        mSecondtWishRecyclerView.setNestedScrollingEnabled(true);
        mWishTitle = (TextView) view.findViewById(R.id.tv_wish_title);
        mCheckWishTitle = (TextView) view.findViewById(R.id.tv_check_wish_title);
        mRelativeLayoutDown1 = (RelativeLayout) view.findViewById(R.id.relative_layout_down1);
        mRelativeLayoutDown2 = (RelativeLayout) view.findViewById(R.id.relative_layout_down2);

        ((MainActivity) getActivity()).showFabButton();

        View.OnClickListener oclRelativeLayoutDown1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // mFirstWishRecyclerView.visi
            }
        };
        mRelativeLayoutDown1.setOnClickListener(oclRelativeLayoutDown1);

        View.OnClickListener oclRelativeLayoutDown2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
        mRelativeLayoutDown2.setOnClickListener(oclRelativeLayoutDown2);

        Bundle bundle1 = this.getArguments();

        if (bundle1 != null) {
            mMenuInWLF = bundle1.getString(MainActivity.mMENU);
            if (mMenuInWLF == "my_wish") {
                mFirstWishList = WishRepository.getObj(getActivity()).getMyWishList();
                mSecondWishList = WishRepository.getObj(getActivity()).getMyCheckWishList();
                mWishTitle.setText("Мои желания");
                mCheckWishTitle.setText("Мои выполненные желания");
                mMenuString = "my_wish_menu";
                mMenuString2 = "my_check_wish_menu";


            } else if (mMenuInWLF == "partner_wish") {
                mFirstWishList = WishRepository.getObj(getActivity()).getPartnerWishList();
                mSecondWishList = WishRepository.getObj(getActivity()).getPartnerCheckWishList();
                mWishTitle.setText("Его желания");
                mCheckWishTitle.setText("Его выполненные желания");
                mMenuString = "partner_wish_menu";
                mMenuString2 = "partner_check_wish_menu";
            }


            mAdapterFirst = new WishListAdapter2(mFirstWishList,mMenuString );
            mFirstWishRecyclerView.setAdapter(mAdapterFirst);

            mAdapterSecond = new WishListAdapter2(mSecondWishList,mMenuString2 );
            mSecondtWishRecyclerView.setAdapter(mAdapterSecond);
        }
        return view;
    }

    public class WishListAdapter2 extends RecyclerView.Adapter<WishListAdapter2.ViewHolder> {

        List<OneWishClass> wishList;
        static final String TAG = "myLogs";
        public static final String EXTRA_WISH_ID = "com.example.alena.wish_id";
        private String mMenuInWLF;

        public WishListAdapter2(List<OneWishClass> wishList, String mMenuInWLF) {
            this.wishList = wishList;
            this.mMenuInWLF = mMenuInWLF;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wish, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            OneWishClass oneWish = wishList.get(position);
            holder.bindWish(oneWish);

        }


        @Override
        public int getItemCount() {
            return wishList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView mTitleTextView;
            private TextView mCommentTextView;
            private ImageView mImageView;
            private OneWishClass mWish;
            private Button mButtonDay;
            private String uriString;
            private Uri myImageUri;

            public ViewHolder(View oneView) {
                super(oneView);


                //mWishTitle = (TextView) oneView.findViewById(R.id.wish_title);

                mTitleTextView = (TextView) oneView.findViewById(R.id.tv_list_item);
                mCommentTextView = (TextView) oneView.findViewById(R.id.tv_comment);
                mImageView = (ImageView) oneView.findViewById(R.id.image_view_one_wish);


                final RatingBar ratingBar_Small = (RatingBar)oneView.findViewById(R.id.ratingbar_Small);


                ratingBar_Small.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){

                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating,
                                                boolean fromUser) {
                        // TODO Auto-generated method stub
                        ratingBar_Small.setRating(rating);
                        ratingBar_Small.setRating(rating);


                    }});


                View.OnClickListener itemOcl = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(),mWish.getTitle()+"checked!)",Toast.LENGTH_SHORT).show();
                        Fragment fragmentOneWish = new AddWishFragment();

                        Bundle bundle = new Bundle();
                        bundle.putString(EXTRA_WISH_ID, mWish.getId().toString());
                        bundle.putString(MainActivity.mMENU, mMenuInWLF);
                        fragmentOneWish.setArguments(bundle);

                        ((MainActivity)getActivity()).placeFragment(fragmentOneWish);

                    }
                };
                oneView.setOnClickListener(itemOcl);
            }

            public void bindWish(OneWishClass oneWishClass){
                mWish =  oneWishClass;
                mTitleTextView.setText(mWish.getTitle());
                mCommentTextView.setText(mWish.getComment());
                mImageView.setImageResource(R.drawable.icecream);

                if (mWish.getImagePath() != null) {
                    Uri imagePathUri = Uri.parse(mWish.getImagePath());
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imagePathUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (bitmap != null) {
                        mImageView.setImageBitmap(bitmap);
                    }
                } else mImageView.setImageResource(R.drawable.icecream);

            }


        }
    }
}