package com.example.alena.wishlist;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class WishListFragment extends Fragment {

    private RecyclerView mWishRecyclerView;
    private WishListAdapter mAdapter;
    List<OneWishClass> mWishList;

    static final String TAG = "myLogs";
    public static final String EXTRA_WISH_ID = "com.example.alena.wish_id";
    //public static final String mMENU = "com.example.alena.wish_list";
    private String mMenuInWLF;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_1_wish_list, container, false);
        mWishRecyclerView = (RecyclerView) view.findViewById(R.id.wishListRecyclerView);
        mWishRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ((MainActivity) getActivity()).showFabButton();

        Bundle bundle1 = this.getArguments();

        if (bundle1!=null) {
            mMenuInWLF = bundle1.getString(MainActivity.mMENU);
            if (mMenuInWLF == "wish_list_menu") {
                mWishList = WishRepository.getObj(getActivity()).getWishList();
            } else if(mMenuInWLF == "my_wish_menu") {
                mWishList = WishRepository.getObj(getActivity()).getMyWishList();
            } else if (mMenuInWLF == "my_check_wish_menu") {
                mWishList = WishRepository.getObj(getActivity()).getMyCheckWishList();
            } else if (mMenuInWLF == "partner_wish_menu") {
                mWishList = WishRepository.getObj(getActivity()).getPartnerWishList();
            } else if (mMenuInWLF == "partner_check_wish_menu") {
                mWishList = WishRepository.getObj(getActivity()).getPartnerCheckWishList();
            }
        }


        mAdapter = new WishListAdapter(mWishList, mMenuInWLF);
        mWishRecyclerView.setAdapter(mAdapter);

        return view;
    }
    public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.ViewHolder> {

        List<OneWishClass> wishList;
        static final String TAG = "myLogs";
        public static final String EXTRA_WISH_ID = "com.example.alena.wish_id";
        private String mMenuInWLF;

        public WishListAdapter(List<OneWishClass> wishList, String mMenuInWLF) {
            this.wishList = wishList;
            this.mMenuInWLF = mMenuInWLF;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wish, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            OneWishClass oneWish = wishList.get(position);
            holder.bindWish(oneWish);
        }

        @Override
        public int getItemCount() {
            return wishList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView mTitleTextView;
            private TextView mCommentTextView;

            private ImageView mImageView;
            private OneWishClass mWish;
            private Button mButtonDay;
            private String uriString;
            private Uri myImageUri;

            public ViewHolder(View oneView) {
                super(oneView);
                //mWishTitle = (TextView) oneView.findViewById(R.id.wish_title);

                mTitleTextView = (TextView) oneView.findViewById(R.id.tv_list_item);
                mCommentTextView = (TextView) oneView.findViewById(R.id.tv_comment);
                mImageView = (ImageView) oneView.findViewById(R.id.image_view_one_wish);

                final RatingBar ratingBar_Small = (RatingBar)oneView.findViewById(R.id.ratingbar_Small);
                ratingBar_Small.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){

                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating,
                                                boolean fromUser) {
                        // TODO Auto-generated method stub
                        ratingBar_Small.setRating(rating);
                        ratingBar_Small.setRating(rating);
                    }});


                View.OnClickListener itemOcl = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Fragment fragmentOneWish = new AddWishFragment();

                        Bundle bundle = new Bundle();
                        bundle.putString(EXTRA_WISH_ID, mWish.getId().toString());
                        bundle.putString(MainActivity.mMENU, mMenuInWLF);
                        fragmentOneWish.setArguments(bundle);

                        ((MainActivity)getActivity()).placeFragment(fragmentOneWish);

                    }
                };
                oneView.setOnClickListener(itemOcl);
            }

            public void bindWish(OneWishClass oneWishClass){
                mWish =  oneWishClass;
                mTitleTextView.setText(mWish.getTitle());
                mCommentTextView.setText(mWish.getComment());
                if (mWish.getImagePath() != null) {
                    Uri imagePathUri = Uri.parse(mWish.getImagePath());
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imagePathUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (bitmap != null) {
                        mImageView.setImageBitmap(bitmap);
                    }
                } else mImageView.setImageResource(R.drawable.icecream);

                // получаю строку с адресом
                //перевожу строку в ссылку
                //задаю картинке эту ссылку
  //              uriString = mWish.getImagePath();
//                myImageUri = Uri.parse(uriString);
 //               mImageView.setImageURI(myImageUri);


            }


        }
    }

}
