package com.example.alena.wishlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by alena on 06.10.2016.
 */
public class DatePickerFragment extends Fragment {

    private DatePicker mDatePicker;
    private Button mButtonDateSave;
    private TextView mTextViewDatePicker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_date_picker, container, false);
//
//        mDatePicker = (DatePicker) view.findViewById(R.id.datePickerDataPickerFragment);
//        mButtonDateSave = (Button) view.findViewById(R.id.btn_date_save);
//        mTextViewDatePicker = (TextView) view.findViewById(R.id.tv_date_picker_fragment);
//
//
//        Calendar today = Calendar.getInstance();
//
//        mTextViewDatePicker.setText(new StringBuilder()
//                // Месяц отсчитывается с 0, поэтому добавляем 1
//                .append(mDatePicker.getDayOfMonth()).append(".")
//                .append(mDatePicker.getMonth() + 1).append(".")
//                .append(mDatePicker.getYear()));
//
//        mDatePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
//                today.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
//                    @Override
//                    public void onDateChanged(DatePicker view, int year,
//                                              int monthOfYear, int dayOfMonth) {
//                        Toast.makeText(getActivity(), "onDataChenged", Toast.LENGTH_SHORT).show();
//                        mTextViewDatePicker.setText("Год: " + year + "/n" + "Месяц: " + (monthOfYear + 1)
//                                + "\n" + "День: " + dayOfMonth);
//                    }
//                });
//
//        View.OnClickListener oclBtnSaveDate =  new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mTextViewDatePicker.setText(new StringBuilder()
//                        // Месяц отсчитывается с 0, поэтому добавляем 1
//                        .append(mDatePicker.getDayOfMonth()).append(".")
//                        .append(mDatePicker.getMonth() + 1).append(".")
//                        .append(mDatePicker.getYear()));
//
//
//                Fragment addWishFragment = new AddWishFragment();
//
//                Bundle bundle = new Bundle();
//                bundle.putString(EXTRA_WISH_ID, mWish.getId().toString());
//                bundle.putString(MainActivity.mMENU, mMenuInWLF);
//                addWishFragment.setArguments(bundle);
//
//                ((MainActivity)getActivity()).placeFragment(addWishFragment);
//            }
//        };
//        mButtonDateSave.setOnClickListener(oclBtnSaveDate);







        return view;
    }
}
