package com.example.alena.wishlist;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

/**
 * Created by alena on 11.10.2016.
 */
public class WishListApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Configuration dbConfiguration = new Configuration.Builder(this)
                .setDatabaseName("wishlist.db")
                .setDatabaseVersion(1)
                .create();
        ActiveAndroid.initialize(dbConfiguration);
    }
}
