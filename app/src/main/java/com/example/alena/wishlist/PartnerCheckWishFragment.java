package com.example.alena.wishlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by alena on 19.09.2016.
 */
public class PartnerCheckWishFragment extends Fragment {

    private RecyclerView mWishRecyclerView;
    private TextView mTitleTextView;
    // private PartnerCheckWishListAdapter mAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partner_check_wish,container,false);

        mWishRecyclerView = (RecyclerView) view.findViewById(R.id.partner_check_wish_recycler_view);
        mTitleTextView = (TextView) view.findViewById(R.id.partner_check_wish_title);

        mWishRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // mAdapter = new PartnerCheckWishListAdapter(mList);
        //mWishRecyclerView.setAdapter(mAdapter);

        return view;
    }
}
