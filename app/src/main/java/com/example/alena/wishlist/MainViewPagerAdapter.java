package com.example.alena.wishlist;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alena on 22.09.2016.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {

    private static final String LOG_TAG = "MainViewPagerAdapter";

    List<Fragment> fragments = new ArrayList<>();
    List<String> titleFragments = new ArrayList<>();

    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleFragments.get(position);
    }

    public void addFragment(int position, Fragment fragment, String titleFragment) {
        fragments.add(position, fragment);
        titleFragments.add(position, titleFragment);
    }

    public List<Fragment> getFragments() {
        return fragments;
    }
}