package com.example.alena.wishlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.SharedPreferencesCompat;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;


import java.net.UnknownHostException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by alena on 30.09.2016.
 */
public class ProfilFragment extends Fragment {
    private EditText mNameUser;
    private EditText mStatus;
    private EditText mMail;
    private EditText mPhone;
    private EditText mVk;
    private CircleImageView mMyPhotoIV;
    private CircleImageView mPartnerPhotoIV;
    private Button mInfoSaveBtn;
    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_NAME = "Nickname";
    public static final String APP_PREFERENCES_STATUS = "Status";
    public static final String APP_PREFERENCES_MAIL = "Mail";
    public static final String APP_PREFERENCES_PHONE = "Phone";
    public static final String APP_PREFERENCES_VK = "Vk";

    private SharedPreferences mSettingSP;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_info_partner, container, false);
        mNameUser = (EditText) view.findViewById(R.id.et_my_name);
        mStatus = (EditText) view.findViewById(R.id.et_status);
        mMail = (EditText) view.findViewById(R.id.et_my_mail);
        mPhone = (EditText) view.findViewById(R.id.et_my_phone);
        mVk = (EditText) view.findViewById(R.id.et_my_vk);
        mMyPhotoIV = (CircleImageView) view.findViewById(R.id.profile_image_info);
        mPartnerPhotoIV = (CircleImageView) view.findViewById(R.id.partner_profile_image_info);
        mInfoSaveBtn = (Button) view.findViewById(R.id.info_save_btn);
        mSettingSP = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        ((MainActivity) getActivity()).hideFabButton();

        User user = new User();
        user = UserRepository.getObj(getActivity()).getAlena();

        showUserInfo(user);

        View.OnClickListener oclInfoSave = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user = UserRepository.getObj(getActivity()).getAlena();
                if (mNameUser.getText().toString().length() > 0){
                    user.setNameUser(mNameUser.getText().toString());

                    SharedPreferences.Editor editor = mSettingSP.edit();
                    editor.putString(APP_PREFERENCES_NAME, mNameUser.getText().toString());
                    editor.commit();
                    Log.v("Profile", "Save namePref:" + mSettingSP.getString(APP_PREFERENCES_NAME, ""));

                    //mSettingSP.edit().putString().commit();
                    editor.putString(APP_PREFERENCES_STATUS, mStatus.getText().toString());
                    editor.commit();
                    Log.v("Profile", "Save StatusPref:" + mSettingSP.getString(APP_PREFERENCES_STATUS, ""));

                    editor.putString(APP_PREFERENCES_MAIL, mMail.getText().toString());
                    editor.commit();
                    editor.putString(APP_PREFERENCES_PHONE, mPhone.getText().toString());
                    editor.commit();
                    editor.putString(APP_PREFERENCES_VK, mVk.getText().toString());
                    editor.commit();
                    Log.v("Profile", "Save namePref:" + mSettingSP.getString(APP_PREFERENCES_NAME, ""));


                }
                if (mStatus.getText().toString().length() > 0){
                    user.setStatus(mStatus.getText().toString());
                }
                if (mMail.getText().toString().length() > 0){
                user.setMail(mMail.getText().toString());
                }
                if (mPhone.getText().toString().length() > 0) {
                    user.setPhone(mPhone.getText().toString());
                }
                if (mVk.getText().toString().length() > 0) {
                    user.setVk(mVk.getText().toString());
                }
                UserRepository.getObj(getActivity()).setAlena(user);

                //Fragment fragmentWishList = new WishListFragment();
                Fragment pageFragment = new PageFragment();
               // Bundle bundle = new Bundle();
               // bundle.putString(MainActivity.mMENU, "wish_list_menu");
               // fragmentWishList.setArguments(bundle);




                        ((MainActivity) getActivity()).placeFragment(pageFragment);
            }

        };
        mInfoSaveBtn.setOnClickListener(oclInfoSave);


        View.OnClickListener oclMyPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user = UserRepository.getObj(getActivity()).getAlena();
                showUserInfo(user);

                mNameUser.setInputType(1);
                mStatus.setInputType(1);
                mMail.setInputType(1);
                mPhone.setInputType(1);
                mVk.setInputType(1);


                ViewGroup sceneRoot = (ViewGroup) view.findViewById(R.id.info_linear_layout);
                CircleImageView partnerPhotoView = (CircleImageView) view.findViewById(R.id.partner_profile_image_info);
                CircleImageView myPhotoView = (CircleImageView) view.findViewById(R.id.profile_image_info);

                int newPartnerPhotoSize = getResources().getDimensionPixelSize(R.dimen.circle_size);
                int newMyPhotoSize = getResources().getDimensionPixelSize(R.dimen.my_circle_size);

                // вызываем метод, говорящий о том, что мы хотим анимировать следующие изменения внутри sceneRoot
                TransitionManager.beginDelayedTransition(sceneRoot);

                //  применение самих изменений
                ViewGroup.LayoutParams params = partnerPhotoView.getLayoutParams();
                ViewGroup.LayoutParams myParams = myPhotoView.getLayoutParams();
                params.width = newMyPhotoSize;
                params.height = newMyPhotoSize;
                myParams.width = newPartnerPhotoSize;
                myParams.height = newPartnerPhotoSize;
                partnerPhotoView.setLayoutParams(params);
            }

        };
        mMyPhotoIV.setOnClickListener(oclMyPhoto);

        View.OnClickListener oclPartnerPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User user = new User();
                user = UserRepository.getObj(getActivity()).getAlena();
                if (mNameUser.getText().toString().length() > 0){
                    user.setNameUser(mNameUser.getText().toString());
                }
                if (mStatus.getText().toString().length() > 0){
                    user.setStatus(mStatus.getText().toString());
                }
                if (mMail.getText().toString().length() > 0){
                    user.setMail(mMail.getText().toString());
                }
                if (mPhone.getText().toString().length() > 0) {
                    user.setPhone(mPhone.getText().toString());
                }
                if (mVk.getText().toString().length() > 0) {
                    user.setVk(mVk.getText().toString());
                }
                UserRepository.getObj(getActivity()).setAlena(user);



                mNameUser.setInputType(0);
                mStatus.setInputType(0);
                mMail.setInputType(0);
                mPhone.setInputType(0);
                mVk.setInputType(0);

                User user2 = new User();
                user2 = UserRepository.getObj(getActivity()).getIgor();
                showUserInfo(user2);





                ViewGroup sceneRoot = (ViewGroup) view.findViewById(R.id.info_linear_layout);
                CircleImageView partnerPhotoView = (CircleImageView) view.findViewById(R.id.partner_profile_image_info);
                CircleImageView myPhotoView = (CircleImageView) view.findViewById(R.id.profile_image_info);

                int newPartnerPhotoSize = getResources().getDimensionPixelSize(R.dimen.circle_size);
                int newMyPhotoSize = getResources().getDimensionPixelSize(R.dimen.my_circle_size);

                // вызываем метод, говорящий о том, что мы хотим анимировать следующие изменения внутри sceneRoot
                TransitionManager.beginDelayedTransition(sceneRoot);

                //  применение самих изменений
                ViewGroup.LayoutParams params = partnerPhotoView.getLayoutParams();
                ViewGroup.LayoutParams myParams = myPhotoView.getLayoutParams();
                params.width = newPartnerPhotoSize;
                params.height = newPartnerPhotoSize;
                myParams.width = newMyPhotoSize;
                myParams.height = newMyPhotoSize;
                partnerPhotoView.setLayoutParams(params);



                // mPartnerPhotoIV.setLayoutParams(new RelativeLayout.LayoutParams(122, 122));
            }
        };
        mPartnerPhotoIV.setOnClickListener(oclPartnerPhoto);

        return view;
    }

    public void showUserInfo(User user){


        if( user.getId() == UserRepository.getObj(getActivity()).getIgor().getId()){
            mNameUser.setText(user.getNameUser());
            mStatus.setText(user.getStatus());
            mMail.setText( user.getMail());
            mPhone.setText( user.getPhone());
            mVk.setText( user.getVk());
            Log.v("Profile", "Show partner name" + user.getNameUser().toString());
        }

        if (user.getId() == UserRepository.getObj(getActivity()).getAlena().getId()){
            if(mSettingSP.contains(APP_PREFERENCES_NAME)){
                mNameUser.setText(mSettingSP.getString(APP_PREFERENCES_NAME,""));
            } else {
                mNameUser.setText(user.getNameUser());            }
            if(mSettingSP.contains(APP_PREFERENCES_STATUS)){
                mStatus.setText(mSettingSP.getString(APP_PREFERENCES_STATUS,""));
            } else {
                mStatus.setText(user.getStatus());            }
            if(mSettingSP.contains(APP_PREFERENCES_MAIL)){
                mMail.setText(mSettingSP.getString(APP_PREFERENCES_MAIL,""));
            }
            if(mSettingSP.contains(APP_PREFERENCES_PHONE)){
                mPhone.setText(mSettingSP.getString(APP_PREFERENCES_PHONE,""));
            }
            if(mSettingSP.contains(APP_PREFERENCES_VK)){
                mVk.setText(mSettingSP.getString(APP_PREFERENCES_VK,""));
            }



            Log.v("Profile", "Show name" + mSettingSP.getString(APP_PREFERENCES_NAME, ""));
        }
        Log.v("Profile", "Show null" + user.getId().toString());
        Log.v("Profile", "Show null my" + user.getId() +UserRepository.getObj(getActivity()).getAlena().getId());
        Log.v("Profile", "Show null partner" + user.getId() + UserRepository.getObj(getActivity()).getIgor().getId());
    }

    @Override
    public void onPause() {
        super.onPause();

        ((MainActivity)getActivity()).showFabButton();
    }

    @Override
    public void onResume() {
        super.onResume();
//
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
