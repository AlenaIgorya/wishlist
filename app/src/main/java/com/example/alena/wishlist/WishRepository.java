package com.example.alena.wishlist;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by alena on 16.09.2016.
 */
public class WishRepository {

    private static WishRepository sWishRepository;
    private List<OneWishClass> mWishList;
    private List<OneWishClass> myWishList;
    private List<OneWishClass> myCheckWishList;
    private List<OneWishClass> partnerWishList;
    private List<OneWishClass> partnerCheckWishList;

    public static WishRepository getObj(Context context) {
        if (sWishRepository == null) {
            sWishRepository = new WishRepository(context);
        }
        return sWishRepository;
    }

    private WishRepository(Context context) {

        mWishList = new ArrayList<OneWishClass>();
        myWishList = new ArrayList<OneWishClass>();
        myCheckWishList = new ArrayList<OneWishClass>();
        partnerWishList = new ArrayList<OneWishClass>();
        partnerCheckWishList = new ArrayList<OneWishClass>();

        OneWishClass ice_cream = new OneWishClass();
        ice_cream.setTitle("ice cream");
        ice_cream.setComment("вафельный рожек =)");
        mWishList.add(ice_cream);

        OneWishClass flowers = new OneWishClass();
        flowers.setTitle("flowers");
        flowers.setComment("Чтобы долго стояли");
        myWishList.add(flowers);

        OneWishClass flowersCheck = new OneWishClass();
        flowersCheck.setTitle("flowers");
        flowersCheck.setComment("Пахли вкусно =)");
        myCheckWishList.add(flowersCheck);

        OneWishClass chocolatePartner = new OneWishClass();
        chocolatePartner.setTitle("chocolate");
        chocolatePartner.setComment("горький...");
        partnerWishList.add(chocolatePartner);

        OneWishClass summerCheckP = new OneWishClass();
        summerCheckP.setTitle("summer");
        summerCheckP.setComment("Солнечное!!!");
        partnerCheckWishList.add(summerCheckP);

        OneWishClass sun = new OneWishClass();
        sun.setTitle("sun");
        sun.setComment("С тобой =*");
        mWishList.add(sun);
    }

    public void saveToList(OneWishClass newWish){

        for (int i=0; i<mWishList.size(); i++) {

            if (mWishList.get(i).getId().equals(newWish.getId())) {
                mWishList.get(i).setTitle(newWish.getTitle());
                return;
            }
        }
        mWishList.add(newWish);
    }
    public void saveToMyList(OneWishClass newWish){

        for (int i=0; i<myWishList.size(); i++) {

            if (myWishList.get(i).getId().equals(newWish.getId())) {
                myWishList.get(i).setTitle(newWish.getTitle());
                return;
            }
        }
        myWishList.add(newWish);
    }
    public void saveToMyCheckList(OneWishClass newWish){

        for (int i=0; i<myCheckWishList.size(); i++) {

            if (myCheckWishList.get(i).getId().equals(newWish.getId())) {
                myCheckWishList.get(i).setTitle(newWish.getTitle());
                return;
            }
        }
        myCheckWishList.add(newWish);
    }
    public void saveToPartnerList(OneWishClass newWish){

        for (int i=0; i<partnerWishList.size(); i++) {

            if (partnerWishList.get(i).getId().equals(newWish.getId())) {
                partnerWishList.get(i).setTitle(newWish.getTitle());
                return;
            }
        }
        partnerWishList.add(newWish);
    }
    public void saveToCheckPartnerList(OneWishClass newWish){

        for (int i=0; i<partnerCheckWishList.size(); i++) {

            if (partnerCheckWishList.get(i).getId().equals(newWish.getId())) {
                partnerCheckWishList.get(i).setTitle(newWish.getTitle());
                return;
            }
        }
        partnerCheckWishList.add(newWish);
    }

    public List<OneWishClass> getWishList() {  return mWishList;  }
    public List<OneWishClass> getMyWishList() {  return myWishList;  }
    public List<OneWishClass> getMyCheckWishList() {  return myCheckWishList;  }
    public List<OneWishClass> getPartnerWishList() {  return partnerWishList;  }
    public List<OneWishClass> getPartnerCheckWishList() {  return partnerCheckWishList; }

    public OneWishClass getWish(UUID id) {
        for (OneWishClass wish : mWishList) {

            if (wish.getId().equals(id)) {
                return wish;
            }
        }
        return null;
    }
    public OneWishClass getMyWish(UUID id) {
        for (OneWishClass wish : myWishList) {

            if (wish.getId().equals(id)) {
                return wish;
            }
        }
        return null;
    }
    public OneWishClass getMyCheckWish(UUID id) {
        for (OneWishClass wish : myCheckWishList) {

            if (wish.getId().equals(id)) {
                return wish;
            }
        }
        return null;
    }
    public OneWishClass getPartnerWish(UUID id) {
        for (OneWishClass wish : partnerWishList) {

            if (wish.getId().equals(id)) {
                return wish;
            }
        }
        return null;
    }
    public OneWishClass getPartnerCheckWish(UUID id) {
        for (OneWishClass wish : partnerCheckWishList) {

            if (wish.getId().equals(id)) {
                return wish;
            }
        }
        return null;
    }
}
