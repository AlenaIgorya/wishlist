package com.example.alena.wishlist;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

/**
 * Created by alena on 17.09.2016.
 */
public class AddWishFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private EditText mAddEditText;
    private Button mSaveButton;
    private TextView mTextViewCommentTitle;
    private TextView mTextViewTimeTitle;
    private TextView mTextViewImportant;
    private UUID mWishId;
    private OneWishClass oneWishClass;
    private FragmentManager fm;
    private String mMenuInWLF;
    private EditText mEditTextComment;

    private ImageView mImageView;
    private String uriString;
    private Uri myImageUri;
    private DatePicker mDatePicker;
    private TextView mTextViewTime;
    private String mImagePathString;


    static final int GALLERY_REQUEST = 1;
    int DIALOG_DATE = 1;

    @Override
    public void onStart() {
        super.onStart();
        Log.v("AddWishFragment", "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.v("AddWishFragment", "onStop");
        ((MainActivity) getActivity()).showFabButton();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v("AddWishFragment", "onCreateView");

        View view = inflater.inflate(R.layout.fragment_add_wish, container, false);

        mAddEditText = (EditText) view.findViewById(R.id.et_add_wish);
        mEditTextComment = (EditText) view.findViewById(R.id.et_comment);
        mSaveButton = (Button) view.findViewById(R.id.btn_save);
        mTextViewCommentTitle = (TextView) view.findViewById(R.id.tv_comment_title);
        mTextViewTimeTitle = (TextView) view.findViewById(R.id.tv_time_title);
        mTextViewTime = (TextView) view.findViewById(R.id.tv_time);
        mTextViewImportant = (TextView) view.findViewById(R.id.tv_important);
        mImageView = (ImageView) view.findViewById(R.id.image_view_add_fragment);

        final RatingBar ratingBar_Indicator = (RatingBar) view.findViewById(R.id.ratingbar_Indicator);

        mTextViewCommentTitle.setText(Html.fromHtml("<u>" + "Комментарий" + "</u>"));
        mTextViewTimeTitle.setText(Html.fromHtml("<u>" + "Время выполнения" + "</u>"));
        mTextViewImportant.setText(Html.fromHtml("<u>" + "Важность" + "</u>"));



        final Bundle bundle = this.getArguments();
        Log.v("AddWish", "bundle:" + bundle);

        if (bundle != null) {
            mMenuInWLF = bundle.getString(MainActivity.mMENU);
            if (bundle.getString(WishListFragment.EXTRA_WISH_ID) != null) {

                mWishId = UUID.fromString(bundle.getString(WishListFragment.EXTRA_WISH_ID));

                if (mMenuInWLF == "wish_list_menu") {
                    oneWishClass = WishRepository.getObj(getActivity()).getWish(mWishId);

                } else if (mMenuInWLF == "my_wish_menu") {
                    oneWishClass = WishRepository.getObj(getActivity()).getMyWish(mWishId);

                } else if (mMenuInWLF == "my_check_wish_menu") {
                    oneWishClass = WishRepository.getObj(getActivity()).getMyCheckWish(mWishId);
                    mAddEditText.setInputType(0); //look for inputType
                    mEditTextComment.setInputType(0);
                    mSaveButton.setText("Назад");

                } else if (mMenuInWLF == "partner_wish_menu") {
                    oneWishClass = WishRepository.getObj(getActivity()).getPartnerWish(mWishId);
                    mAddEditText.setInputType(0); //look for inputType
                    mEditTextComment.setInputType(0);
                    mSaveButton.setText("Назад");

                } else if (mMenuInWLF == "partner_check_wish_menu") {
                    oneWishClass = WishRepository.getObj(getActivity()).getPartnerCheckWish(mWishId);
                    mAddEditText.setInputType(0); //look for inputType
                    mEditTextComment.setInputType(0);
                    mSaveButton.setText("Назад");
                }
                mAddEditText.setText(oneWishClass.getTitle());
                mEditTextComment.setText(oneWishClass.getComment());
                mTextViewTime.setText(oneWishClass.getDate());

                if (oneWishClass.getImagePath() != null) {
                    Uri imagePathUri = Uri.parse(oneWishClass.getImagePath());
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imagePathUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (bitmap != null) {
                        mImageView.setImageBitmap(bitmap);
                    }
                }

//                // получаю строку с адресом
//                //перевожу строку в ссылку
//                //задаю картинке эту ссылку
//                if (oneWishClass != null) {
//                    if (uriString != null) {
//                        uriString = oneWishClass.getImagePath();
//
//                        Log.v("AddWishFragment", "uriString: " + uriString);
//
//                        myImageUri = Uri.parse(uriString);
//                        mImageView.setImageURI(myImageUri);
//                    }
//                }
            }

            View.OnClickListener oclBtnSave = new View.OnClickListener() {


                @Override
                public void onClick(View v) {
                    if (oneWishClass == null) {
                        oneWishClass = new OneWishClass();
                    }
                    if (mAddEditText.getText().toString().length() > 0) {
                        oneWishClass.setTitle(mAddEditText.getText().toString());
                        oneWishClass.setComment(mEditTextComment.getText().toString());
                        oneWishClass.setDate(mTextViewTime.getText().toString());
                        if (mImagePathString!=null){
                            oneWishClass.setImagePath(mImagePathString);
                        }

                        if (mEditTextComment.getText().toString().length() > 0) {
                            oneWishClass.setComment(mEditTextComment.getText().toString());
                        }
                        if (mMenuInWLF == "wish_list_menu") {
                            WishRepository.getObj(getActivity()).saveToList(oneWishClass);
                        } else if (mMenuInWLF == "my_wish_menu") {
                            WishRepository.getObj(getActivity()).saveToMyList(oneWishClass);
                        } else if (mMenuInWLF == "my_check_wish_menu") {
                            WishRepository.getObj(getActivity()).saveToMyCheckList(oneWishClass);
                        } else if (mMenuInWLF == "partner_wish_menu") {
                            WishRepository.getObj(getActivity()).saveToPartnerList(oneWishClass);
                        } else if (mMenuInWLF == "partner_check_wish_menu") {
                            WishRepository.getObj(getActivity()).saveToCheckPartnerList(oneWishClass);
                        }
                    }


                    Log.v("AddWishFragment", "myLog replace fragment");
                    // Fragment fragmentWishList = new WishListFragment();
                    Fragment pageFragment = new PageFragment();
                    pageFragment.setArguments(bundle);
                    ((MainActivity) getActivity()).placeFragment(pageFragment);
                }
            };

            mSaveButton.setOnClickListener(oclBtnSave);


        }
        View.OnClickListener oclTVTime = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        };

        mTextViewTime.setOnClickListener(oclTVTime);

//        mTextViewTime.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                setCurrentDateOnView();
//                return true;
//            }
//        });


        View.OnClickListener oclAddPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);

            }
        };
        mImageView.setOnClickListener(oclAddPhoto);


        ratingBar_Indicator.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                // TODO Auto-generated method stub
                ratingBar_Indicator.setRating(rating);
            }
        });
        return view;
    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment(this);
        newFragment.show(getChildFragmentManager(), "datePicker");
    }


//    public void setCurrentDateOnView() {
//        final Calendar calendar = Calendar.getInstance();
//        int year = calendar.get(Calendar.YEAR);
//        int month = calendar.get(Calendar.MONTH);
//        int day = calendar.get(Calendar.DAY_OF_MONTH);
//
//        // set current date into textview
//        mTextViewTime.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(day).append(".").append(month + 1).append(".")
//                .append(year));
//
//        // Устанавливаем текущую дату для DatePicker
//        mDatePicker.init(year, month, day, null);
//    }


    @Override
    public void onPause() {
        super.onPause();
        Log.v("AddWishFragment", "onPause");
      //  ((MainActivity) getActivity()).showFabButton();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("AddWishFragment", "onResume");
        ((MainActivity) getActivity()).hideFabButton();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        Log.v("AddWishFragment", "onActivityResult");

        Bitmap bitmap = null;

        switch (requestCode) {
            case GALLERY_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    Log.v("AddWishFragment", "selectedImage: " + selectedImage);
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    mImageView.setImageBitmap(bitmap);

                    if (oneWishClass!=null){
                        oneWishClass.setImagePath(selectedImage.toString());
                        mImagePathString = selectedImage.toString();
                    }


                    Log.v("AddWishFragment", "selectedImage: " + selectedImage.toString());

                }
        }
    }


    public void onDateSet(DatePicker view, int year, int month, int day) {

        Log.v("DATEPIckER", "date: " + year + " " + month + " " + day);

        mTextViewTime.setText("date: " + year + "-" + month + "-" + day);

    }

    public static class DatePickerFragment extends DialogFragment {

        DatePickerDialog.OnDateSetListener callback;

        public DatePickerFragment(DatePickerDialog.OnDateSetListener callback) {
            this.callback = callback;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), callback, year, month, day);

        }
    }
}



