package com.example.alena.wishlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by alena on 19.09.2016.
 */
public class MyWishFragment extends Fragment {

    private RecyclerView mWishRecyclerView;
    private TextView mTitleTextView;
    private MyWishListAdapter mAdapter;
    static final String TAG = "myLogs";
    public static final String EXTRA_WISH_ID = "com.example.alena.wish_id";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_wish,container,false);

        mWishRecyclerView = (RecyclerView) view.findViewById(R.id.my_wish_recycler_view);
        mTitleTextView = (TextView) view.findViewById(R.id.my_wish_title);

        mWishRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
       // mAdapter = new MyWishListAdapter(mList);
        //mWishRecyclerView.setAdapter(mAdapter);

        return view;
    }

    private class MyWishListAdapter extends RecyclerView.Adapter<MyWishListAdapter.ViewHolder> {

        List<OneWishClass> wishList;

        public MyWishListAdapter(List<OneWishClass> wishList) {

            this.wishList = wishList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wish, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            OneWishClass oneWish = wishList.get(position);
            //holder.mWishTitle.setText(oneWish.getTitle());
            holder.bindWish(oneWish);

        }

        @Override
        public int getItemCount() {
            return wishList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private OneWishClass mWish;

            public ViewHolder(View oneView) {
                super(oneView);

                mTitleTextView = (TextView) oneView.findViewById(R.id.tv_list_item);
                View.OnClickListener itemOcl = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(),mWish.getTitle()+"checked!)",Toast.LENGTH_SHORT).show();

                        Fragment fragmentOneWish = new AddWishFragment();

                        Bundle bundle = new Bundle();
                        bundle.putString(EXTRA_WISH_ID, mWish.getId().toString());
                        fragmentOneWish.setArguments(bundle);

                        ((MainActivity) getActivity()).placeFragment(fragmentOneWish);                    }
                };
                oneView.setOnClickListener(itemOcl);
            }



            public void bindWish(OneWishClass oneWishClass){
                mWish =  oneWishClass;
                mTitleTextView.setText(mWish.getTitle());

                Log.d(TAG, "Biiiiiind oneWishClass");
            }
        }
    }
}


