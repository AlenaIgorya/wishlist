package com.example.alena.wishlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static final String TAG = "myLogs";
    static final int PAGE_COUNT = 5;
    public static String menuString;
    private FragmentManager fm;
    private ViewPager mViewPager;
    MainViewPagerAdapter pagerAdapter;
    public static final String mMENU = "com.example.alena.wish_list";
    private SharedPreferences mSetting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "myLogs onCreate");

        mSetting = getSharedPreferences(ProfilFragment.APP_PREFERENCES, Context.MODE_PRIVATE);

        fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);

        if (fragment == null) {

            AuthorizationFragment authorizationFragment = new AuthorizationFragment();
            placeFragment(authorizationFragment);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        View headerLayout = navigationView.getHeaderView(0);
        CircleImageView myPhotoImageView = (CircleImageView) headerLayout.findViewById(R.id.profile_image);
        TextView mMyNameET = (TextView) headerLayout.findViewById(R.id.nav_header_my_name);
        TextView mMyStatusET = (TextView) headerLayout.findViewById(R.id.nav_header_status);

        User userI = new User();
        userI = UserRepository.getObj(this).getAlena();
        //mMyNameET.setText(userI.getNameUser());

        mMyStatusET.setText(userI.getStatus());

        FloatingActionButton fabButton = (FloatingActionButton) findViewById(R.id.fab);
        View.OnClickListener oclBtnFab = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragmentAddWish = new AddWishFragment();
                Bundle bundle = new Bundle();
                menuString = "my_wish_menu";
                bundle.putString(mMENU, menuString);
                fragmentAddWish.setArguments(bundle);
                placeFragment(fragmentAddWish);
            }
        };

        fabButton.setOnClickListener(oclBtnFab);


        View.OnClickListener oclMyPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragmentProfil = new ProfilFragment();
                placeFragment(fragmentProfil);
                drawer.closeDrawer(GravityCompat.START);
            }
        };
        myPhotoImageView.setOnClickListener(oclMyPhoto);

    }

    public void hideFabButton() {

        FloatingActionButton fabButton = (FloatingActionButton) findViewById(R.id.fab);
        fabButton.setVisibility(View.GONE);

    }

    public void showFabButton() {
        FloatingActionButton fabButton = (FloatingActionButton) findViewById(R.id.fab);
        fabButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);
        TextView mMyNameET = (TextView) headerLayout.findViewById(R.id.nav_header_my_name);
        mSetting = getSharedPreferences(ProfilFragment.APP_PREFERENCES, Context.MODE_PRIVATE);
        if(mSetting.contains(ProfilFragment.APP_PREFERENCES_NAME)){
            mMyNameET.setText(mSetting.getString(ProfilFragment.APP_PREFERENCES_NAME,""));
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        int containeger = R.id.fragmentContainer;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Log.d(TAG, "myLogs setting");
            Toast.makeText(this, "R.id.setting", Toast.LENGTH_SHORT).show();

            AuthorizationFragment authorizationFragment = new AuthorizationFragment();

//            menuString = "setting";
//
//            Bundle bundle1 = new Bundle();
//            bundle1.putString(mMENU, menuString);
//            authorizationFragment.setArguments(bundle1);


            this.getSupportFragmentManager().beginTransaction()
                    .replace(containeger, authorizationFragment)
                    .addToBackStack(null)
                    .commit();


        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        Log.v(TAG, "onNavItemSelectted");

        int id = item.getItemId();
        int containeger = R.id.fragmentContainer;


        if (id == R.id.main_page_menu) {

            Log.d(TAG, "myLogs menu my");
            // Toast.makeText(this, "R.id.main_page_menu", Toast.LENGTH_SHORT).show();
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("WishList");

            PageFragment pageFragment = new PageFragment();
            menuString = "main_page_menu";

            this.getSupportFragmentManager().beginTransaction()
                    .replace(containeger, pageFragment)
                    .addToBackStack(null)
                    .commit();


//            this.getSupportFragmentManager().beginTransaction()
//                    .replace(containeger, wishListFragment, menuString)
//                    .addToBackStack(null)
//                    .commit();


        } else if (id == R.id.my_wish_menu) {

            Log.d(TAG, "myLogs menu my");
            // Toast.makeText(this, "id == R.id.my_wish_menu", Toast.LENGTH_SHORT).show();

            WishListFragment wishListFragment = new WishListFragment();
            menuString = "my_wish_menu";

            Bundle bundle1 = new Bundle();
            bundle1.putString(mMENU, menuString);
            wishListFragment.setArguments(bundle1);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Мои желания");


            this.getSupportFragmentManager().beginTransaction()
                    .replace(containeger, wishListFragment)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.my_check_wish_menu) {

            Log.d(TAG, "myLogs menu my CHECK");
            // Toast.makeText(this, "R.id.my_check_wish_menu", Toast.LENGTH_SHORT).show();

            WishListFragment wishListFragment = new WishListFragment();
            menuString = "my_check_wish_menu";

            Bundle bundle1 = new Bundle();
            bundle1.putString(mMENU, menuString);
            wishListFragment.setArguments(bundle1);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Мои выполненные желания");
            // toolbar.setSubtitleTextAppearance(size,10);

            this.getSupportFragmentManager().beginTransaction()
                    .replace(containeger, wishListFragment)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.partner_wish_menu) {

            Log.d(TAG, "myLogs menu PARTNER");
            // Toast.makeText(this, "R.id.partner_wish_menu", Toast.LENGTH_SHORT).show();

            WishListFragment wishListFragment = new WishListFragment();
            menuString = "partner_wish_menu";

            Bundle bundle1 = new Bundle();
            bundle1.putString(mMENU, menuString);
            wishListFragment.setArguments(bundle1);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Его желания");


            this.getSupportFragmentManager().beginTransaction()
                    .replace(containeger, wishListFragment)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.partner_check_wish_menu) {

            //Log.d(TAGMENUPARTNERCHECK , "myLogs ARTNER CHECK");
            //    Toast.makeText(this, "R.id.partner_check_wish_menu", Toast.LENGTH_SHORT).show();

            WishListFragment wishListFragment = new WishListFragment();
            menuString = "partner_check_wish_menu";

            Bundle bundle1 = new Bundle();
            bundle1.putString(mMENU, menuString);
            wishListFragment.setArguments(bundle1);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Его выполненные желания");

            this.getSupportFragmentManager().beginTransaction()
                    .replace(containeger, wishListFragment)
                    .addToBackStack(null)
                    .commit();
        }
        if (id == R.id.nav_share) {

            ProfilFragment profilFragment = new ProfilFragment();

            this.getSupportFragmentManager().beginTransaction()
                    .replace(containeger, profilFragment)
                    .addToBackStack(null)
                    .commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void placeFragment(Fragment fragment) {

        fm.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(null)
                .commit();
    }




}

