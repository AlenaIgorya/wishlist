package com.example.alena.wishlist;

import android.text.Editable;

import com.activeandroid.Model;

import java.util.UUID;

/**
 * Created by alena on 15.09.2016.
 */
public class OneWishClass {

    private UUID mId;
    private String mTitle;
    private String mComment;
    private String mDate;
    private String mImagePath;

    public OneWishClass() {

// Генерирование уникального идентификатора
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getComment() {return mComment;}

    public String getImagePath(){return mImagePath;}

    public String getDate(){return mDate;}


    public void setTitle(String title) {
        mTitle = title;
    }

    public void setComment(String comment) {mComment = comment;}

    public void setImagePath (String imagePath) {mImagePath = imagePath;}

    public void setDate (String date) {mDate = date;}


}
