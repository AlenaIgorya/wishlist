package com.example.alena.wishlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by alena on 16.09.2016.
 */
public class OneWishFragment extends Fragment {

    private OneWishClass mWish;
    private EditText mTitleField;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mWish = new OneWishClass();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_one_wish, container, false);

        mTitleField = (EditText) v.findViewById(R.id.wish_title);
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //намеренно оставленное пустое место
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mWish.setTitle(container.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

                //пусто и не просто так
            }
        });

        return v;
    }
}
