package com.example.alena.wishlist;

import java.util.UUID;

/**
 * Created by alena on 02.10.2016.
 */
public class User {

    private UUID mId;
    private String mNameUser;
    private String mStatus;
    private String mMail;
    private String mPhone;
    private String mVk;
    private String mIdentifier;

    public User() {

// Генерирование уникального идентификатора
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getNameUser() {
        return mNameUser;
    }
    public String getStatus() {return mStatus;}
    public String getMail() {return mMail;}
    public String getPhone() {return mPhone;}
    public String getVk() {return mVk;}
    public String getIdentifier() {return mIdentifier;}


    public void setNameUser(String name) {mNameUser = name;}
    public void setStatus(String status) {mStatus = status;}
    public void setMail(String mail) {mMail = mail;}
    public void setPhone(String number) {mPhone = number;}
    public void setVk(String vk){mVk = vk;}
    public void setIdentifier(String identifier){mIdentifier = identifier;}


}
