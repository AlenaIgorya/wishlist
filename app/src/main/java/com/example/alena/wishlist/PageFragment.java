package com.example.alena.wishlist;

import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by alena on 14.09.2016.
 */
public class PageFragment extends Fragment {

    static final String  ARGUMENT_PAGE_NUMBER = "arg_page_number";
    private View rootView;
    int pageNumber;
    int backColor;
    private static final String LOG_TAG = "PagerFragment";

   // private MyWishFragment newMyWishFragment;
   // private PartnerWishFragment newPartnerWishFragment;
   // private WishListFragment wishListFragment;

    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    private List<OneWishClass> mMyList;
    private List<OneWishClass> mMyCheckList;
    private List<OneWishClass> mPartnerList;
    private List<OneWishClass> mPartnerCheckList;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_demo_view_pager, container, false);

        initMainTabs();
        return rootView;

    }

   // public void onView

    private void initMainTabs() {

        Log.v(LOG_TAG, "initMainTabs");

        mTabLayout = (TabLayout) rootView.findViewById(R.id.m_fragment_tabs);
        mViewPager = (ViewPager) rootView.findViewById(R.id.view_pager);

        mMyList = WishRepository.getObj(getActivity()).getMyWishList();
        mMyCheckList = WishRepository.getObj(getActivity()).getMyCheckWishList();
        mPartnerList = WishRepository.getObj(getActivity()).getPartnerWishList();
        mPartnerCheckList = WishRepository.getObj(getActivity()).getPartnerCheckWishList();

        final MainViewPagerAdapter mainViewPagerAdapter = new MainViewPagerAdapter(getChildFragmentManager());

       // WishListFragment myWishListFragment = new WishListFragment();
        WishListTwoFragment myWishListTwoFragment = new WishListTwoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.mMENU, "my_wish");
        myWishListTwoFragment.setArguments(bundle);
        mainViewPagerAdapter.addFragment(0, myWishListTwoFragment, getString(R.string.my_wish_text));

       // WishListFragment partnerWishListFragment = new WishListFragment();
        WishListTwoFragment partnerWishListTwoFragment = new WishListTwoFragment();
        Bundle bundlePartner = new Bundle();
        bundlePartner.putString(MainActivity.mMENU, "partner_wish");
        partnerWishListTwoFragment.setArguments(bundlePartner);
        mainViewPagerAdapter.addFragment(1, partnerWishListTwoFragment, getString(R.string.his_wish_text));

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mViewPager.setAdapter(mainViewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }


}













